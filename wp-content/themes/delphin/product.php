<?php
/*
    Template Name: product
*/
?>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дельфин | Дельфин 5</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
        <meta name="robots" content="noindex, nofollow">
        <link rel="canonical" href="..." /> 
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
        <!-- styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
        <!-- scripts -->
        <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>
    </head>
    <body>
        <div id="app">
            <!--include modals-->
            <aside class="modals" id="modals"></aside>
            <!--include header-->
            <?php get_header() ?>
            <main>
                <div class="--gradient-bg">
                    <div class="container">
                        <!--start.n-header-->
                        <div class="n-header">
                            <h1 class="font-weight-bold">Дельфин 5</h1> 
                            <div class="col-md-6">
                                <nav aria-label="breadcrumb"class="n-header_breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Главная</a></li>
                                        <li class="breadcrumb-item active"><a href="<?php bloginfo('url') ?>/production.php">Продукция</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Дельфин 5</li>
                                    </ol>
                                </nav>
                            </div>
                        </div><!--end.n-header-->

                        <div class="row controlls justify-content-between align-items-center">
                            <div class="col-md-6 d-flex justify-content-between pb-3">
                                <div class="controlls_mark_group">
                                    <svg class="controlls_svg"xmlns="http://www.w3.org/2000/svg" viewBox="352.998 336 40.57 40.688">
                                        <path id="star" class="star" d="M19493,221.595l13.564-1.763,6.627-15.161,6.84,15.161,13.539,1.763-10.082,8.532,3.313,15.231-13.609-8.462-13.328,8.462,3.244-15.231Z" transform="translate(-19140 131.329)"/>
                                    </svg>
                                    <span class="controlls_stars">10</span>
                                    <span class="controlls_marks">2 оценки</span>
                                </div>
                                <button class="controlls_mark-btn">Оценить товар</button>
                            </div>

                            <div class="col-md-6 d-flex justify-content-between pb-3">
                                <span class="controlls_price"><span class="font-weight-bold">19 438</span> грн.</span>
                                <button class="controlls_buy-btn --box-shadow mr-1">Купить</button>
                                <button class="controlls_compare-btn">Добавить к сравнению</button>
                            </div>
                        </div><!--end.controlls-->

                        <!--start.item-->
                        <section class="item">
                            <div class="row">
                                <div class="col-12 col-lg-6 mb-4">
                                    <div class="row pb-3">
                                        <div class="col">
                                            <img class=" w-100 --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/ship1.jpg" alt="Дельфин 5">
                                        </div>
                                    </div>
                                    <div class="row pb-3">
                                        <div class="col-4">
                                            <img class="w-100 --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/ship2.jpg" alt="Дельфин 5">
                                        </div>
                                        <div class="col-4">
                                            <img class="w-100 --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/ship3.jpg" alt="Дельфин 5">
                                        </div>
                                        <div class="col-4">
                                            <img class="w-100 --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/ship4.jpg" alt="Дельфин 5">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <ul class="nav nav-tabs item_aside_nav">
                                        <li><a class="active show" id="descrTab" data-toggle="tab" href="#description">Описание</a></li>
                                        <li><a data-toggle="tab" id="charsTab" href="#chars">Характеристики</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="description" class="tab-pane fade active show">
                                            <div class="row">
                                                <div class="col-12 mb-4">
                                                    <div class="item_aside_description">
                                                        <p>Новая эксклюзивная модель прикормочных корабликов "Дельфин" не имеющая аналогов.</p>
                                                        <p>Особенностью данного кораблика являеться вращающаяся кормушка, во время ее вращения прикормка разлетаеться в радиусе 5 метров, что позволяет создовать большое пятно закормки в сравнении с обычной кормушкой.</p>
                                                        <p>В добавок к вращающейся кормушки Дельфин 5 имеет бункер для прикормки с нижним сбросом. Кораблик оснащен двумя бесколлекторными моторами с плавной регулировкой скорости, что обеспечивает большую грузоподьемность, разворот на месте.</p>
                                                        <p>Наряду с такими широкими возможностями, как доставка прикормки в любое понравившееся рыбаку место, легкость управления, маневренность, водонепроницаемость корпуса, надежная защита винта от водорослей и простота использования вы получаете удовольствие, рыбалка становиться более продуктивной и увлекательной.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="chars" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="item_aside_description">
                                                        <p>Новая эксклюзивная модель прикормочных корабликов "Дельфин" не имеющая аналогов.</p>
                                                        <p>Особенностью данного кораблика являеться вращающаяся кормушка, во время ее вращения прикормка разлетаеться в радиусе 5 метров, что позволяет создовать большое пятно закормки в сравнении с обычной кормушкой.</p>
                                                        <p>В добавок к вращающейся кормушки Дельфин 5 имеет бункер для прикормки с нижним сбросом. Кораблик оснащен двумя бесколлекторными моторами с плавной регулировкой скорости, что обеспечивает большую грузоподьемность, разворот на месте.</p>
                                                        <p>Наряду с такими широкими возможностями, как доставка прикормки в любое понравившееся рыбаку место, легкость управления, маневренность, водонепроницаемость корпуса, надежная защита винта от водорослей и простота использования вы получаете удовольствие, рыбалка становиться более продуктивной и увлекательной.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!--end.tab-content-->
                                </div><!--end.col-md-6-->
                            </div>
                        </section><!--end.item-->

                        <!--start.additional-->
                        <section class="additional pb-5">
                            <div class="row">
                                <div class="col-12 col-lg-4 mb-4">
                                    <!--start.p-comments-->
                                    <div class="p-comments --box-shadow h-100">
                                        <h2 class="--txt-shadow">Комментарии</h2><span class="p-comments_count">2</span>
                                        <p class="p-comments_hint">Комментарии могут оставлять только пользователи, купившие товар.</p>
                                        <div class="leave-comment --bg-white">
                                            <textarea class="textarea-white w-100" placeholder="Ваш комментарий..."></textarea>
                                            <div class="row flex-nowrap justify-content-between pr-4 pl-4">
                                                <span class="leave-comment_mark">Ваша оценка</span>
                                                <div class="d-flex flex-row align-items-center justify-content-end leave-comment_stars">
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <defs>
                                                            <style>
                                                            .cls-1 {
                                                                fill: none;
                                                                stroke: #000;
                                                                opacity: 0.3;
                                                            }
                                                            </style>
                                                        </defs>
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <img src="<?php bloginfo('template_url') ?>/assets/images/star.svg">
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                    <svg class="star-svg" xmlns="http://www.w3.org/2000/svg" viewBox="495.271 1255.359 20.475 19.544">
                                                        <path class="cls-1" d="M19493,211.874l6.023-.75,2.943-6.453,3.039,6.453,6.014.75-4.479,3.632,1.471,6.483-6.045-3.6-5.918,3.6,1.439-6.483Z" transform="translate(-18996.498 1051.877)"/>
                                                    </svg>
                                                </div>
                                            </div>
                                            <button class="send-btn --bg-blue-light">Отправить</button>
                                            
                                        </div>
                                        <div class="p-comment">
                                            <div class="p-comment_top-group d-flex flex-row align-items-center justify-content-between">
                                                <h3 class="p-comment_author">Андрей Иванов</h3>
                                                <div class="p-comment_count d-flex flex-row align-items-center ">
                                                    <svg class="controlls_svg"xmlns="http://www.w3.org/2000/svg" viewBox="352.998 336 40.57 40.688">
                                                        <path id="star" class="star" d="M19493,221.595l13.564-1.763,6.627-15.161,6.84,15.161,13.539,1.763-10.082,8.532,3.313,15.231-13.609-8.462-13.328,8.462,3.244-15.231Z" transform="translate(-19140 131.329)"/>
                                                    </svg>
                                                    <span class="controlls_stars">10</span>
                                                </div>
                                            </div>
                                            <time class="p-comment_date">16 февраля 2018 г.</time>
                                            <p>При выборе кораблика для рыбалки необходимо исходить из возможностей, которые предоставляет кораблик, и обязательно учитывать Ваши потребности. И тогда с легкостью получится подобрать максимально удачный вариант лично для Вас.</p>
                                        </div>
                                        <div class="p-comment">
                                            <div class="p-comment_top-group d-flex flex-row align-items-center justify-content-between">
                                                <h3 class="p-comment_author">Андрей Иванов</h3>
                                                <div class="p-comment_count d-flex flex-row align-items-center ">
                                                    <svg class="controlls_svg"xmlns="http://www.w3.org/2000/svg" viewBox="352.998 336 40.57 40.688">
                                                        <path id="star" class="star" d="M19493,221.595l13.564-1.763,6.627-15.161,6.84,15.161,13.539,1.763-10.082,8.532,3.313,15.231-13.609-8.462-13.328,8.462,3.244-15.231Z" transform="translate(-19140 131.329)"/>
                                                    </svg>
                                                    <span class="controlls_stars">10</span>
                                                </div>
                                            </div>
                                            <time class="p-comment_date">16 февраля 2018 г.</time>
                                            <p>Отличный кораблик!</p>
                                        </div>
                                    </div><!--start.p-comments-->
                                </div>
                                <div class="col-12 col-lg-8">
                                    <!--start.p-sets-->
                                    <div class="sets --box-shadow">
                                        <h2>Доступные комплектации:</h2>
                                        <p class="sets_hint">* Стоимость указана с учётом установки</p>
                                        <h3 class="sets_category">Эхолоты</h3>
                                        <!--start.sets_echo-->
                                        <div class="sets_echo">
                                            <div class="row flex-wrap">
                                                <div class="col-md-6 mb-4 mt-4">
                                                    <a href="#"><h4 class="sets_heading">Беспроводной  эхолот Lucky FF 918</h4></a>
                                                    <div class="sets_box d-flex flex-row align-items-center justify-content-between">
                                                        <a href="#" class="sets_box_img"><img class="--box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/echo.jpg" alt="Беспроводной  эхолот Lucky FF 918"></a>
                                                        <div class="sets_box_aside">
                                                            <a class="sets_box_description" href="#">описание товара</a>
                                                            <p class="--txt-green">+ 6 310 грн.</p>
                                                            <button class="add-to-cart-btn">Добавить к заказу</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-4 mt-4">
                                                    <a href="#"><h4 class="sets_heading">Беспроводной  эхолот Lucky FF 918</h4></a>
                                                    <div class="sets_box d-flex flex-row align-items-center justify-content-between">
                                                        <a href="#" class="sets_box_img"><img class="--box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/echo.jpg" alt="Беспроводной  эхолот Lucky FF 918"></a>
                                                        <div class="sets_box_aside  ">
                                                            <a class="sets_box_description" href="#">описание товара</a>
                                                            <p class="--txt-green">+ 6 310 грн.</p>
                                                            <button class="add-to-cart-btn">Добавить к заказу</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-4 mt-4">
                                                    <a href="#"><h4 class="sets_heading">Беспроводной  эхолот Lucky FF 918</h4></a>
                                                    <div class="sets_box d-flex flex-row align-items-center justify-content-between">
                                                        <a href="#" class="sets_box_img"><img class="--box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/echo.jpg" alt="Беспроводной  эхолот Lucky FF 918"></a>
                                                        <div class="sets_box_aside  ">
                                                            <a class="sets_box_description" href="#">описание товара</a>
                                                            <p class="--txt-green">+ 6 310 грн.</p>
                                                            <button class="add-to-cart-btn">Добавить к заказу</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--end.sets_echo-->
                                        <!--start.sets_accessories-->
                                        <h3 class="sets_category">Аксессуары</h3>
                                        <div class="sets_accessories">
                                            <div class="row">
                                                <div class="col-md-6 mb-4 mt-4">
                                                    <a href="#"><h4 class="sets_heading">Беспроводной  эхолот Lucky FF 918</h4></a>
                                                    <div class="sets_box d-flex flex-row align-items-center justify-content-between">
                                                        <a href="#" class="sets_box_img"><img class="--box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/echo.jpg" alt="Беспроводной  эхолот Lucky FF 918"></a>
                                                        <div class="sets_box_aside  ">
                                                            <a class="sets_box_description" href="#">описание товара</a>
                                                            <p class="--txt-green">+ 6 310 грн.</p>
                                                            <button class="add-to-cart-btn">Добавить к заказу</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-4 mt-4">
                                                    <a href="#"><h4 class="sets_heading">Беспроводной  эхолот Lucky FF 918</h4></a>
                                                    <div class="sets_box d-flex flex-row align-items-center justify-content-between">
                                                        <a href="#" class="sets_box_img"><img class="--box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/production/echo.jpg" alt="Беспроводной  эхолот Lucky FF 918"></a>
                                                        <div class="sets_box_aside  ">
                                                            <a class="sets_box_description" href="#">описание товара</a>
                                                            <p class="--txt-green">+ 6 310 грн.</p>
                                                            <button class="add-to-cart-btn">Добавить к заказу</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end.sets_accessories-->
                                        <div class="sets_delivery">
                                            <h2>Комплект поставки:</h2>
                                            <ol>
                                                <li>Кораблик - 1 шт</li>
                                                <li>Пульт управления - 1 шт</li>
                                                <li>Батарейка для пульта управления 1.5 В AA - 4 шт</li>
                                                <li>Зарядное устройство Imax-B6 - 1 шт</li>
                                                <li>Сумка для транспортировки - 1 шт</li>
                                            </ol>
                                        </div>
                                    </div><!--start.p-sets-->
                                </div>
                            </div>
                        </section><!--end.additional-->
                    </div><!--end.container-->
                </div><!--end.gradient-bg-->
            </main>
            <!--include footer-->
            <?php get_footer() ?>
        </div><!--end#app-->
    </body>
</html>
