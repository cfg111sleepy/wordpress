<a href="./compare.html" class="modals_side-btn --box-shadow modals_compare">
    <img src="<?php bloginfo('template_url') ?>/assets/images/compare.png" alt="Сравнение">
    <p class="--txt-green d-none d-xl-block">Сравнение</p>
</a>
<a href="./cart.html" class="modals_side-btn --box-shadow modals_tray">
    <img src="<?php bloginfo('template_url') ?>/assets/images/tray.png" alt="Корзина">
    <p class="--txt-green d-none d-xl-block">Корзина</p>
</a>