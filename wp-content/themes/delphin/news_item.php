<?php
/*
    Template Name: news_item
*/
?><html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Дельфин | Как выбрать кораблик для рыбалки</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="..." /> 
    <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
    <!-- styles -->
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
    <!-- scripts -->
    <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
    <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
    <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>
</head>
<body>
    <div id="app">
        <!--include modals-->
        <aside class="modals" id="modals"></aside>
        <!--include header-->
        <?php get_header() ?>
        <main>
            <div class="pattern-bg">
                <!--start.news-item-->
                <section class="news-item">
                    <div class="container --box-shadow --bg-white">
                        <!--start.n-header-->
                        <div class="n-header row pl-4">
                            <h1>Как выбрать кораблик для рыбалки</h1> 
                            <div class="col-md-6">
                                <nav aria-label="breadcrumb"class="n-header_breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Главная</a></li>
                                        <li class="breadcrumb-item active"><a href="<?php bloginfo('url') ?>/index.php/news_list/">Новости</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Как выбрать кораблик для рыбалки</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <!--end.n-header-->
                        <!--start.news_images-->
                        <div class="row">
                            <div class="col-md-7">
                                <img class="img-fluid --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/news_list/news_item1.jpg">
                            </div>
                            <div class="col-md-5 d-none d-md-block">
                                <img class="img-fluid --box-shadow mb-4" src="<?php bloginfo('template_url') ?>/assets/images/news_list/news_item2.jpg">
                                <img class="img-fluid --box-shadow" src="<?php bloginfo('template_url') ?>/assets/images/news_list/news_item3.jpg">
                            </div>
                        </div>
                        <!--end.news_images-->
                        <!--start.news_text-->
                        <div class="row pt-5 pb-5">
                            <div class="col-12">
                                <p>Основные требования</p>   
                                <p>Кораблик для рыбалки должен завозить прикормку и снасть на расстоянии больше 150 м в любых погодных условиях, на расстояние недоступное для ручного заброса.</p>
                                <p> Кораблик должен иметь достаточный запас хода, надежно работать продолжительный период времени, чтобы не подвести в самый ответственный момент.</p>
                                <p> Для комфортного использования кораблика на дистанции больше 150 метров, он должен обладать скоростью движения не меньше 80 м/минуту, иначе будет сложно определить, движется кораблик или стоит на месте. Чем дальше объект, тем сложнее определить его скорость.</p>
                                <p> Корпус должен иметь размеры не меньше 0,5 м, желательно продолговатой формы для того, чтобы вы могли определить направление движения (когда кораблик отдаляется он выглядит как точка, когда стоит как тире).</p>
                                <p>Выбирайте кораблики яркой расцветки, так как на больших дистанциях их лучше видно. Какого бы ни был цвета кораблик, рыба видит его как темное пятно на фоне яркого солнца.</p>
                                <p>Очень важно, чтобы задний ход был полноценный, поскольку часто возникает потребность в его использовании. Скорость, с которой кораблик движется назад, должна незначительно отличатся от движения вперед, иначе, заехав в водоросли, вы не сможете из них выбраться.</p>
                                <p>Обязательно на кораблике должна быть установлена защита от водорослей. Рассчитывать на 100% защиту не стоит. Определенно можно сказать, что более эффективнее работает защита, у которой есть забор воды сбоку, а не только снизу. Если же забор воды только снизу, то когда кораблик сядет на мель, он не сможет из неё выбраться.</p> 
                            </div>
                        </div>
                        <!--end.news_text-->
                        <section class="comments">
                            <h2>Комментарии</h2>
                            <textarea name="comment" id="comment" rows="10" placeholder="Ваш комментарий..."></textarea>
                            <button class="blue-rounbded-TP-btn">Отправить</button>
                            <div class="comments-feed">
                                <div class="comment">
                                    <h3 class="comment_author">Андрей Иванов</h3>
                                    <span class="comment_date d-block d-md-inline">16 февраля 2018 г.</span>
                                    <p class="comment_text">При выборе кораблика для рыбалки необходимо исходить из возможностей, которые предоставляет кораблик, и обязательно учитывать Ваши потребности. И тогда с легкостью получится подобрать максимально удачный вариант лично для Вас.</p>
                                    <button class="comment_submit">Ответить</button>
                                    <div class="comment_reply">
                                        <h3 class="comment_author">Иван Сергеев 
                                            <img src="<?php bloginfo('template_url') ?>/assets/images/news_list/arrow.png">
                                            Андрей Иванов</h3>
                                        <span class="comment_date d-block d-md-inline">16 февраля 2018 г.</span>
                                        <p class="comment_text">При выборе кораблика для рыбалки необходимо исходить из возможностей, которые предоставляет кораблик, и обязательно учитывать Ваши потребности. </p>
                                        <button class="comment_submit">Ответить</button>
                                    </div>
                                </div>
                                <div class="comment">
                                    <h3 class="comment_author">Игорь Тарасов</h3>
                                    <span class="comment_date d-block d-md-inline">16 февраля 2018 г.</span>
                                    <p class="comment_text">Отличная статья!</p>
                                    <button class="comment_submit">Ответить</button>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>
                <!--end.news-item-->  
            </div>
        </main>
        <!--include footer-->
        <?php get_footer() ?>
    </div><!--end#app-->
</body>
</html>