<?php
/*
    Template Name: news_list
*/
?><html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дельфин | Новости</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
        <meta name="robots" content="noindex, nofollow">
        <link rel="canonical" href="..." /> 
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
        <!-- styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
        <!-- scripts -->
        <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>
    </head>
    <body>
        <div id="app">
            <!--include modals-->
            <aside class="modals" id="modals"></aside>
            <!--include header-->
            <?php get_header() ?>
            <main>
                <div class="n-background">
                    <!--start.n-background-->
                    <section class="n-header">
                        <div class="container">
                            <h1>Новости</h1> 
                            <div class="col-md-6">
                                <nav aria-label="breadcrumb"class="n-header_breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Главная</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Новости</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </section>
                    <!--end.n-background-->
                    <!--start.n-feed-->
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post() ?>
                        <section class="n-feed">
                            <div class="container">
                                <div class="row">
                                    <div class="n-feed_item d-flex flex-column flex-md-row --box-shadow">
                                        <div class="col-md-5 d-flex flex-column justify-content-center">
                                            <div class="row">
                                                <a href="<?php the_permalink(); ?>" class="n-feed_item_img">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/news_list/n-item_1.jpg">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="n-feed_item_text">
                                                <a href="<?php the_permalink(); ?>">
                                                    <h4 class="--n-heading"><?php the_title(); ?></h4>
                                                </a>
                                                <time class="n-feed_item_text_time">16 февраля 2018 г.</time>
                                                <p class="n-feed_item_text_post"><?php the_content(); ?></p>
                                                <a href="<?php the_permalink(); ?>" class="n-details-btn">Подробнее</a>
                                                <p class="n-feed_item_text_tags"><strong>Теги:</strong> 
                                                    <?php the_tags(); ?>
                                                </p>
                                                <span class="n-feed_item_text_views d-flex flex-row align-items-center justify-content-end">
                                                    <img class="n-view" src="<?php bloginfo('template_url') ?>/assets/images/news_list/eye.png">
                                                    <span class="n-count">25</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </section>

                        <?php endwhile ?>

                        <?php else: echo "not"; ?>

                        <?php endif;?>

                        <!--end.n-feed-->
                    <!--start.btn-block-->
                    <div class="btn-block">
                        <div class="container">
                            <button class="green-btn">
                                <span class="green-btn_text">Больше</span>
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="911.801 1643.813 107.578 79.651">
                                    <defs>
                                        <style>
                                        .cls-2 {
                                            fill: none;
                                            stroke: #fff;
                                            stroke-width: 3px;
                                        }
                                        </style>
                                    </defs>
                                    <g  transform="matrix(1, 0, 0, 1, 0, 0)">
                                        <path id="arrow-2" data-name="arrow" class="cls-2" d="M13634,1117l22.91,16.584,22.908-16.584" transform="translate(-12696.32 553.03)"/>
                                    </g>
                                    </svg>
                            </button>
                        </div>
                    </div>
                    <!--end.btn-block-->
                    <!--start.n-lead-->
                    <section class="n-lead">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="n-heading">Теги:</h4>
                                    <div class="n-lead_tags">
                                        <a class="n-lead_tags_item" href="#">рыбалка</a>
                                        <a class="n-lead_tags_item" href="#">кораблик</a>
                                        <a class="n-lead_tags_item" href="#">дельфин</a>
                                        <a class="n-lead_tags_item" href="#">карп</a>
                                        <a class="n-lead_tags_item" href="#">амур</a>
                                        <a class="n-lead_tags_item" href="#">рыбалка</a>
                                        <a class="n-lead_tags_item" href="#">кораблик</a>
                                        <a class="n-lead_tags_item" href="#">дельфин</a>
                                        <a class="n-lead_tags_item" href="#">карп</a>
                                        <a class="n-lead_tags_item" href="#">амур</a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="lead --box-shadow d-flex flex-column justify-content-between mb-4">
                                        <h3 class="--txt-shadow">ПОЛУЧИТЕ ВАШУ ПЕРСОНАЛЬНУЮ СКИДКУ НА НАБОР "ДЕЛЬФИН"!</h3>
                                        <p class="--txt-shadow">Промокод на скидку 40% будет отправлен на указанный вами адрес.</p>
                                        <div>
                                            <input type="email" placeholder="Ваш E-mail">
                                            <button type="button" class="red-btn">Получить скидку</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--end.n-lead-->
                    <!--start.n-lead-->
                    <section class="n-gallery">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 pr-4 mb-5">
                                    <h2 class="--a-heading --box-shadow">Видеогалерея</h2>
                                    <div class="n-gallery_container" id="vGallery">
                                        <div class="n-gallery_container_inner" >
                                            <div class="row mb-3">
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/7y5XkxNHgjI" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/7y5XkxNHgjI/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/4DcTqawtPkw" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/4DcTqawtPkw/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/tdP54H0Ane0" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/tdP54H0Ane0/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/tdP54H0Ane0" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/tdP54H0Ane0/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/7y5XkxNHgjI" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/7y5XkxNHgjI/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                                <div class="n-gallery_item">
                                                    <a href="https://www.youtube.com/embed/7y5XkxNHgjI" target="modalVid" class="video_aside_preview">
                                                        <img src="https://img.youtube.com/vi/7y5XkxNHgjI/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                                        <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h2 class="--a-heading --box-shadow">Фотогалерея</h2>
                                    <div class="n-gallery_container" id="phGallery">
                                        <div class="n-gallery_container_ph_inner">
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/ph-gal_1.jpg">
                                            </div>
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/n-item_1.jpg">
                                            </div>
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/n-item_2.jpg">
                                            </div>
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/n-item_3.jpg">
                                            </div>
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/ph-gal_1.jpg">
                                            </div>
                                            <div class="n-gallery_ph_item">
                                                <img class="n-gallery_ph_item_img" src="<?php bloginfo('template_url') ?>/assets/images/news_list/ph-gal_1.jpg">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--end.n-lead-->
                </div>
                <!--end.n-background-->
            </main>
            <!--include footer-->
            <?php get_footer() ?>
        </div><!--end#app--> 
        <!--start.n-modal_vid-->
        <section class="n-modal_vid">
            <div class="container">
                <div class="modalVid">
                    <button class="modalVid_close-btn">
                        <svg class="blue-close-svg" xmlns="http://www.w3.org/2000/svg" viewBox="887.005 254 16.668 16.668">
                            <g id="del1" class="cls-1" transform="translate(887.005 254)">
                            <g id="Ellipse_15" data-name="Ellipse 15" class="cls-2" transform="translate(0)">
                                <circle class="cls-4" cx="8.334" cy="8.334" r="8.334"/>
                                <circle class="cls-5" cx="8.334" cy="8.334" r="7.834"/>
                            </g>
                            <line id="Line_23" data-name="Line 23" class="cls-3" y2="10.79" transform="translate(12.234 4.604) rotate(45)"/>
                            <line id="Line_24" data-name="Line 24" class="cls-3" x2="10.79" transform="translate(4.604 4.604) rotate(45)"/>
                            </g>
                        </svg>
                    </button>
                    <iframe id="modalVid" name="modalVid" class="--box-shadow" width="95%" src="" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </section><!--end.n-modal_vid-->
        <!--start.n-modal_ph-->
        <section class="n-modal_ph modalPh" id="modalPh">
            <button class="modalVid_close-btn">
                <svg class="blue-close-svg" xmlns="http://www.w3.org/2000/svg" viewBox="887.005 254 16.668 16.668">
                    <g id="del1" class="cls-1" transform="translate(887.005 254)">
                        <g id="Ellipse_15" data-name="Ellipse 15" class="cls-2" transform="translate(0)">
                        <circle class="cls-4" cx="8.334" cy="8.334" r="8.334"/>
                        <circle class="cls-5" cx="8.334" cy="8.334" r="7.834"/>
                        </g>
                        <line id="Line_23" data-name="Line 23" class="cls-3" y2="10.79" transform="translate(12.234 4.604) rotate(45)"/>
                        <line id="Line_24" data-name="Line 24" class="cls-3" x2="10.79" transform="translate(4.604 4.604) rotate(45)"/>
                    </g>
                </svg>
            </button>
            <div class="owl-carousel owl-theme owl-news_list">
            </div>
        </section><!--end.n-modal_ph-->
    </body>
</html>