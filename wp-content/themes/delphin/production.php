<?php
/*
    Template Name: production
*/
?>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дельфин | Продукция</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
        <meta name="robots" content="noindex, nofollow">
        <link rel="canonical" href="..." /> 
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
        <!-- styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
        <!-- scripts -->
        <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>
    </head>
    <body>
        <div id="app">
            <!--include modals-->
            <aside class="modals" id="modals"></aside>
            <!--include header-->
            <?php get_header() ?>
            <main>
                <div class="pattern-bg">
                    <div class="container">
                        <!--start.n-header-->
                        <div class="n-header pl-4">
                            <h1>Продукция</h1> 
                            <div class="col-md-6">
                                <nav aria-label="breadcrumb"class="n-header_breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Главная</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Продукция</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                        <!--end.n-header-->
                        <section class="production">
                            <ul class="nav nav-tabs --box-shadow">
                                <li><a class="active show" data-toggle="tab" href="#menu1">Кораблики</a></li>
                                <li><a data-toggle="tab" href="#menu2">Эхолоты</a></li>
                                <li><a data-toggle="tab" href="#menu3">Аксессуары</a></li>
                                <li><a data-toggle="tab" href="#menu4">Запчасти</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="menu1" class="tab-pane fade active show">
                                    <div class="row">
                                    <div class="col-12 col-sm-6 col-md-3 mb-4">
                                        <a href="<?php bloginfo('url') ?>/index.php/production/product/">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                        <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                        <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                        <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                        <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                        <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                        <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/ship.png" alt="Дельфин 5">
                                                    <p class="text-center mb-0 production_box_line">Дельфин 5</p>
                                                    <span class="--txt-green text-center d-block">19 438 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--end.menu1-->
                                <div id="menu2" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/toslon1.jpg" alt="Toslon TF500">
                                                    <p class="text-center mb-0 production_box_line">Toslon TF500</p>
                                                    <span class="--txt-green text-center d-block">9699 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/toslon1.jpg" alt="Toslon TF500">
                                                    <p class="text-center mb-0 production_box_line">Toslon TF500</p>
                                                    <span class="--txt-green text-center d-block">9699 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/toslon1.jpg" alt="Toslon TF500">
                                                    <p class="text-center mb-0 production_box_line">Toslon TF500</p>
                                                    <span class="--txt-green text-center d-block">9699 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/toslon1.jpg" alt="Toslon TF500">
                                                    <p class="text-center mb-0 production_box_line">Toslon TF500</p>
                                                    <span class="--txt-green text-center d-block">9699 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div> <!--end.menu2-->
                                <div id="menu3" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/delfin_bag.jpg" alt='Сумка "Дельфин"'>
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">1972 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/delfin_bag.jpg" alt='Сумка "Дельфин"'>
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">1972 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/delfin_bag.jpg" alt='Сумка "Дельфин"'>
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">1972 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/delfin_bag.jpg" alt='Сумка "Дельфин"'>
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">1972 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div> <!--end.menu3-->
                                <div id="menu4" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/screw.jpg" alt="Винт">
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">80 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/screw.jpg" alt="Винт">
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">80 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/screw.jpg" alt="Винт">
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">80 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-12 col-sm-6 col-md-3 mb-4">
                                            <a href="#">
                                                <div class="production_box --box-shadow">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/production/screw.jpg" alt="Винт">
                                                    <p class="text-center mb-0 production_box_line">Сумка "Дельфин"</p>
                                                    <span class="--txt-green text-center d-block">80 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div><!--end.row-->
                                </div> <!--end.menu4-->
                            </div><!--end.tab-content-->
                        </section><!--end.production-->
                    </div><!--end.container-->
                </div>
                <!--end.pattern-bg-->
            </main>
            <!--include footer-->
            <?php get_footer() ?>
        </div><!--end#app-->
    </body>
</html>