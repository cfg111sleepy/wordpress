<div class="header-line">
    <div class="container align-items-center">
        <div class="row">
            <div class="col-md-12 col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start flex-wrap">
                Оформите заказ до <span class="time">12.00 </span>
                <img class="d-none d-xl-block" src="<?php bloginfo('template_url') ?>/assets/images/box.png" alt="order"> 
                и мы отправим его сегодня!
                <img class="d-none d-xl-block" src="<?php bloginfo('template_url') ?>/assets/images/deliver.png" alt="order"> 
            </div>
            <div class="col-md-12 col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end">
                <span class="green-text">Осталось:</span>
                <div class="timer d-flex flex-row tk-europa">
                    <div class="timer_cell">
                        <span class="timer_integer" id="h1">0</span>
                        <span class="timer_integer" id="h2">0</span>
                    </div>
                    <div class="timer_cell">
                        <span class="timer_integer" id="m1">0</span>
                        <span class="timer_integer" id="m2">0</span>
                    </div>
                    <div class="timer_cell timer_sec">
                        <span class="timer_integer" id="s1">0</span>
                        <span class="timer_integer" id="s2">0</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end.header-line-->
<div class="header">
    <div class="container">
        <div class="row justify-content-between align-items-center flex-row-reverse flex-sm-row pr-5 pl-4 p-sm-0">
            <div class="header_logo order-5 order-sm-0">
                <a href="<?php bloginfo('url') ?>"><img src="<?php bloginfo('template_url') ?>/assets/images/header_logo_delf.png" alt="Delfin"></a>
            </div>
            <nav class="header_navigation navbar navbar-expand-lg order-sm-5 order-lg-1" >
                <button class="navbar-toggler" id="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 53 53" style="enable-background:new 0 0 53 53;" xml:space="preserve" width="100%" height="100%" fill="ffffff"><g><g>
                            <g>
                                <path d="M2,13.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,13.5,2,13.5z" data-original="#000000" class="active-path" data-old_color="#FDF6F6" fill="#FDFBFB"/>
                                <path d="M2,28.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,28.5,2,28.5z" data-original="#000000" class="active-path" data-old_color="#FDF6F6" fill="#FDFBFB"/>
                                <path d="M2,43.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,43.5,2,43.5z" data-original="#000000" class="active-path" data-old_color="#FDF6F6" fill="#FDFBFB"/>
                            </g>
                        </g></g> </svg>
                    </span>
                </button>
                <div class="header_lg-navabar d-none d-lg-flex">
                    <ul class="navbar-nav mr-auto">
                        <li><a href="<?php bloginfo('url') ?>">Главная</a></li>
                        <li><a href="<?php bloginfo('url') ?>/index.php/production/">Продукция</a></li>
                        <li><a href="<?php bloginfo('url') ?>/index.php/news_list/">Новости</a></li>
                        <li><a href="<?php bloginfo('url') ?>/index.php/about/">О компании</a></li>
                        <li><a href="<?php bloginfo('url') ?>/index.php/contact/">Контакты</a></li>
                    </ul>
                </div>  
            </nav>
            <div class="header_search d-none d-sm-flex justify-content-end flex-row order-sm-1 order-lg-2">
                <input type="text" placeholder="Поиск по сайту">
                <button class="search-btn">
                    <img src="<?php bloginfo('template_url') ?>/assets/images/search.png" alt="search" title="Найти">
                </button>
            </div>
            <div class="header_tel flex-column justify-content-center d-none d-xl-flex order-lg-3">
                    <a href="tel:+380990444030">+38 (099) 044-4030</a>
                    <a href="tel:+380982801111">+38 (098) 280-1111</a> 
            </div>
            <div class="header_options order-sm-3 order-lg-4">
                <select name="language" id="language">
                    <option value="ru" selected>RUS</option>
                    <option value="ru">UKR</option>
                </select>
            </div>
            <div class="header_options order-sm-4 order-lg-5">
                <select name="currency" id="currency">
                    <option value="uah" selected>UAH</option>
                    <option value="rur">RUR</option>
                    <option value="usd">USD</option>
                </select>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li><a href="<?php bloginfo('url') ?>">Главная</a></li>
                <li><a href="<?php bloginfo('url') ?>/index.php/production/">Продукция</a></li>
                <li><a href="<?php bloginfo('url') ?>/index.php/news_list/">Новости</a></li>
                <li><a href="<?php bloginfo('url') ?>/index.php/about/">О компании</a></li>
                <li><a href="<?php bloginfo('url') ?>/index.php/contact/">Контакты</a></li>
            </ul>
        </div> 
    </div>
</div>
<!--end.header-->