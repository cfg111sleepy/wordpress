<?php
/*
    Template Name: contact
*/
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Дельфин | Контакты</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
    <meta name="robots" content="noindex, nofollow">
    <link rel="canonical" href="..." /> 
    <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
    <!-- styles -->
    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
    <!-- scripts -->
    <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
    <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
    <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>
</head>
<body>
    <div id="app">
        <!--include modals-->
        <aside class="modals" id="modals"></aside>
        <!--include header-->
        <?php get_header() ?>
        <main>
            <div class="pattern-bg">
                <!--start.c-map-->
                <section class="c-map --gradient-rad">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-md-7">
                                <h1>Наши магазины</h1>
                                <img class="img-fluid d-md-none"src="<?php bloginfo('template_url') ?>/assets/images/news_list/map_mobile.png" alt="Наши магазины на карте">
                                <img class="img-fluid d-none d-md-block" src="<?php bloginfo('template_url') ?>/assets/images/contact_map.png" alt="Наши магазины на карте">
                                <!--start.cities-->
                                <div class="c-map_cities d-none d-md-block">
                                    <div class="Kiev cityBlock" id="Kiev">
                                        <span class="city" id="Kiev">Киев</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Sumy cityBlock" id="Sumy">
                                        <span class="city" id="Sumy">Сумы</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Kharkov cityBlock" id="Kharkov">
                                        <span class="city" id="Kharkov">Харьков</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Starobelsk cityBlock" id="Starobelsk">
                                        <span class="city" id="Starobelsk">Старобельск</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Severodonetsk cityBlock d-flex flex-column-reverse" id="Severodonetsk">
                                        <span class="city" id="Severodonetsk">Северодонецк</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Melitopol cityBlock" id="Melitopol">
                                        <span class="city" id="Melitopol">Мелитополь</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Herson cityBlock d-flex flex-column-reverse" id="Herson">
                                        <span class="city" id="Herson">Херсон</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Nikolaev cityBlock" id="Nikolaev">
                                        <span class="city" id="Nikolaev">Николаев</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Odessa cityBlock" id="Odessa">
                                        <span class="city" id="Odessa">Одесса</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Kamenets cityBlock d-flex flex-column-reverse" id="Kamenets">
                                        <span class="city" id="Odessa">Каменец-Подольский</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Lvov cityBlock" id="Lvov">
                                        <span class="city" id="Lvov">Львов</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Khmelnitskiy cityBlock" id="Khmelnitskiy">
                                        <span class="city" id="Khmelnitskiy">Хмельницкий</span>
                                        <span class="dot"></span>
                                    </div>
                                    <div class="Moscow cityBlock" id="Moscow">
                                        <span class="city" id="Moscow">Москва</span>
                                        <span class="dot"></span>
                                    </div>
                                    
                                </div>
                                <!--end.cities-->
                                <div class="c-map_overlay">
                                    <div class="row d-flex flex-column align-items-center justify-content-around text-center h-100">
                                            <h3 class="--txt-green" id="cityName">Северодонецк</h3>
                                            <ul>
                                                <li id="storeName"></li>
                                                <li id="storeAddress"></li>
                                                <li id="telNumber"></li>
                                                <li id="name"></li>
                                            </ul>
                                            <ul>
                                                <li id="addStore"></li>
                                                <li id="addTel"></li>
                                                <li id="addName"></li>
                                            </ul>
                                            <div class="" id="storeImg"></div>
                                            <button class="c-map_overlay_submit" id="contactClose">Назад</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end.c-map-->
                <!--start.c-address-->
                <section class="c-address">
                    <div class="container">
                        <div class="row ml-3">
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Главный офис "Суперпринтер"</h2>
                                <ul>
                                    <li>г. Северодонецк</li>
                                    <li>пер. Ломоносова 8-а</li>
                                    <li>+38 (098) 380 - 11 - 11</li>
                                    <li>+38 (099) 044 - 40 - 30</li>
                                    <li>Артем</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "Goodfish"</h2>
                                <ul>
                                    <li>г. Харьков</li>
                                    <li>ул. Григоровское шоссе 63</li>
                                    <li>+38 (050) 051 - 03 - 81</li>
                                    <li>+38 (098) 027 - 04 - 81</li>
                                    <li>Аким</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>"Наш ринок" магазин №255</h2>
                                <ul>
                                    <li>г. Львов</li>
                                    <li>ул. Рапапорта 8</li>
                                    <li>+38 (067) 938 - 93 - 08</li>
                                    <li>Юра</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин - "Carp Time"</h2>
                                <ul>
                                    <li>г. Сумы</li>
                                    <li>ул. Перекопская 9</li>
                                    <li>+38 (050) 947 - 89 - 89</li>
                                    <li>Сергей</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row ml-3">
                            <div class="col-12 col-sm-6 col-md-3 ">
                                <h2>Магазин "Zebra"</h2>
                                <ul>
                                    <li>г. Николаев</li>
                                    <li>ул. Чкалова 35б</li>
                                    <li>+38 (093) 532 - 61 - 11</li>
                                    <li>Стас</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "Дом Рыбака"</h2>
                                <ul>
                                    <li>г. Старобельск</li>
                                    <li>ул. Кирова 1</li>
                                    <li>+38 066 331 - 75 - 99</li>
                                    <li>Артем</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "ПОКЛЕВКА"</h2>
                                <ul>
                                    <li>г. Херсон</li>
                                    <li>бульвар Мирный 4</li>
                                    <li>+38 (067) 553 - 45 - 67</li>
                                    <li>+38 (066) 188 - 57 - 55</li>
                                    <li>Евгений</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "Рыболов"</h2>
                                <ul>
                                    <li>г. Каменец-Подольский</li>
                                    <li>ул. Князей Кориатовичей 13</li>
                                    <li>+38 (096) 273 - 68 - 63</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row ml-3">
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "Карп"</h2>
                                <ul>
                                    <li>г. Хмельницкий</li>
                                    <li>ул. Подольская 93</li>
                                    <li>Павел</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин «Novitex»</h2>
                                <ul>
                                    <li>г. Одесса</li>
                                    <li>Малиновский рынок павильон 74-Б</li>
                                    <li>+38 (096) 777-36-66</li>
                                    <li>+38 (095) 645-75-75</li>
                                    <li>novitex.com.ua</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Магазин "ПОКЛЕВКА"</h2>
                                <ul>
                                    <li>г. Херсон</li>
                                    <li>бульвар Мирный 4</li>
                                    <li>+38 (067) 553 - 45 - 67</li>
                                    <li>+38 (066) 188 - 57 - 55</li>
                                    <li>Евгений</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Интернет Магазин</h2>
                                <ul>
                                    <li>Россия г. Москва</li>
                                    <li>84954090896, ‎‎89099651350 WhatsApp, Viber</li>
                                    <li>delfin-boat.ru</li>
                                </ul>
                            </div>
                        </div>
                        <div class="row ml-3">
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Продажа - Сервис</h2>
                                <ul>
                                    <li>г. Киев</li>
                                    <li>+38 (050) 511 511 8</li>
                                    <li>Сергей</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Продажа - Сервис</h2>
                                <ul>
                                    <li>г. Хмельницкий</li>
                                    <li>+38 (098) 625 75 30</li>
                                    <li>Максим</li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-md-3">
                                <h2>Продажа - Сервис</h2>
                                <ul>
                                    <li>г. Мелитополь</li>
                                    <li>+ 38 (067) 11 33 998</li>
                                    <li>Сергей</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end.c-address-->
            </div>
        </main>
        <!--include footer-->
        <?php get_footer() ?>
    </div><!--end#app-->
</body>
</html>