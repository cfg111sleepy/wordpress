<?php
function wpdocs_theme_name_scripts() {
    wp_enqueue_style( 'script-name', get_template_directory_uri() . 'assets/css/main.css');
    wp_enqueue_style( 'script-name', get_template_directory_uri() . 'libs/dist/css/bootstrap-grid.css');
    wp_enqueue_style( 'script-name', get_template_directory_uri() . 'libs/dist/css/bootstrap.css');

}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

?>  