<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дельфин</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
        <meta name="robots" content="noindex, nofollow">
        <link rel="canonical" href="..." /> 
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
        <!-- styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
        <!-- scripts -->
        <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>

    </head>
    <body>
        <div id="app">
            <!--include modals-->
            <!--include header-->
            <?php get_header()?>
            
            <main>
                <section class="banner">
                    <div class="banner_overlay">
                        <div class="container">
                            <h1>КОРАБЛИК ДЛЯ ПРИКОРМКИ "DELFIN"</h1>
                            <h2>умный помощник рыболова</h2>
                            <div class="row d-none d-md-flex">
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/map.png" alt="map">
                                        <p>Идеальная траектория движения</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/aim.png" alt="map">
                                        <p>Точность автопилота до 1 метра</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/use.png" alt="map">
                                        <p>Максимальная простота использования и обслуживания</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/guarantee.png" alt="map">
                                        <p>Гарантия 2 года</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/ten_years.png" alt="map">
                                        <p>С 2010 года на рынке</p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="banner_feature-box d-flex flex-column justify-content-around align-items-center mb-4">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/quality.png" alt="map">
                                        <p>Лучшее соотношение цена-качество</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end.banner-->
                <section class="goods">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="lead --box-shadow d-flex flex-column justify-content-between mb-4">
                                    <h3 class="--txt-shadow">ПОЛУЧИТЕ ВАШУ ПЕРСОНАЛЬНУЮ СКИДКУ НА НАБОР "ДЕЛЬФИН"!</h3>
                                    <p class="--txt-shadow">Промокод на скидку 40% будет отправлен на указанный вами адрес.</p>
                                    <div>
                                        <input type="email" placeholder="Ваш E-mail">
                                        <button type="button" class="red-btn">Получить скидку</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <a href="#">
                                    <div class="goods_together --box-shadow d-flex flex-column align-items-center mb-4">
                                        <p class="goods_together_caption">Вместе дешевле!</p>
                                        <img class="goods_together_boat img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/boat.png">
                                        <span class="goods_together_plus">+</span>
                                        <img class="goods_together_acc img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/acc.png">
                                        <p class="goods_together_price">
                                            Дельфин 5 + Источник питания
                                            <span class="--txt-red d-block text-center"> 20 000 грн.</span>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="popular">
                            <h4 class="--subheading"> Популярные товары:</h4>
                            <div class="row">
                                <div class="col-md-3">
                                    <a href="#">
                                        <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4 new">
                                            <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/acc.png">
                                            <p class="popular_together_price">
                                                    Источник питания
                                                <span class="--txt-green d-block text-center"> 
                                                    <span class="old-price"></span>
                                                    600 грн.
                                                </span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#">
                                        <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4 new">
                                            <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/boat.png">
                                            <p class="popular_together_price">
                                                    Дельфин 5
                                                <span class="--txt-green d-block text-center"> 
                                                    <span class="old-price"></span>
                                                    19 438 грн.
                                                </span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#">
                                        <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4 action">
                                            <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/screw.png">
                                            <p class="popular_together_price">
                                                Винт
                                                <span class="--txt-green d-block text-center"> 
                                                    <span class="old-price">90 грн.</span>
                                                    80 грн.
                                                </span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a href="#">
                                    <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4">
                                            <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/holder.png">
                                            <p class="popular_together_price">
                                                    Держатель эхолота
                                                <span class="--txt-green d-block text-center"> 
                                                    <span class="old-price"></span>
                                                    318 грн.
                                                </span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--end.popular-->
                    </div>
                </section>
                <!--end.goods-->
                <section class="work --overlay-blue">
                    <div class="container">
                        <h4>Как мы работаем:</h4>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="work_box d-flex flex-column justify-content-between align-items-center">
                                    <div class="work_image-box">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/order.png" alt="order">
                                    </div>
                                    <p>Вы офрмляете заказ на сайте</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="work_box d-flex flex-column justify-content-between align-items-center">
                                    <div class="work_image-box">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/manager.png" alt="manager">
                                    </div>
                                    <p>С вами связывается наш менеджер для уточнения деталей</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="work_box d-flex flex-column justify-content-between align-items-center">
                                    <div class="work_image-box">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/deliver_work.png" alt="deliver">
                                    </div>
                                    <p>В течении 2-3 дней мы доставляем заказ в любую точку Украины</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="work_box d-flex flex-column justify-content-between align-items-center">
                                    <div class="work_image-box">
                                        <img src="<?php bloginfo('template_url') ?>/assets/images/pay.png" alt="pay">
                                    </div>
                                    <p>Вы оплачиваете товар при получении либо предоплатой на карту ПриватБанка</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end.work-->
                <section class="video pt-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 video_aside_wrapper">
                                <h4 class="--subheading ml-4">Наши видео: </h4>
                                <div class="video_aside d-flex flex-row d-md-block" id="vids_aside">
                                    <div class="row mb-3">
                                        <a href="https://www.youtube.com/embed/7y5XkxNHgjI" target="main-vid" class="video_aside_preview">
                                            <img src="https://img.youtube.com/vi/7y5XkxNHgjI/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                            <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                        </a>
                                    </div>
                                    <div class="row mb-3">
                                        <a href="https://www.youtube.com/embed/4DcTqawtPkw" target="main-vid" class="video_aside_preview">
                                            <img src="https://img.youtube.com/vi/4DcTqawtPkw/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                            <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                        </a>
                                    </div>
                                    <div class="row mb-3">
                                        <a href="https://www.youtube.com/embed/tdP54H0Ane0" target="main-vid" class="video_aside_preview">
                                            <img src="https://img.youtube.com/vi/tdP54H0Ane0/mqdefault.jpg" alt="Overlook RC Fishing Boat" class="yt_preview">
                                            <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                        </a>
                                    </div>
                                    <div class="row mb-3">
                                        <a href="https://www.youtube.com/embed/tdP54H0Ane0" target="main-vid" class="video_aside_preview">
                                            <img src="https://img.youtube.com/vi/tdP54H0Ane0/mqdefault.jpg" alt="Карповый кораблик Дельфин-2L для рыбалки. Обзор.">
                                            <button class="ytp-large-play-button ytp-button" aria-label="Відтворити"><svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%"><path class="ytp-large-play-button-bg" d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z"></path><path d="M 45,24 27,14 27,34" fill="#fff"></path></svg></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row video_main-vid">
                                    <iframe id="main-vid" name="main-vid" class="--box-shadow" width="95%" height="370" src="https://www.youtube.com/embed/tdP54H0Ane0" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div class="video_kits">
                                    <h4 class="--subheading mt-4">Наборы</h4>
                                    <div class="row justify-content-between">
                                        <div class="col-sm-6 col-lg-3">
                                            <a href="#">
                                                <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/kit.jpg" alt="kit">
                                                    <span class="--txt-green text-center">600 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-lg-3">
                                            <a href="#">
                                                <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/kit.jpg" alt="kit">
                                                    <span class="--txt-green text-center">600 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-lg-3">
                                            <a href="#">
                                                <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/kit.jpg" alt="kit">
                                                    <span class="--txt-green text-center">600 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-sm-6 col-lg-3">
                                            <a href="#">
                                                <div class="popular_box --box-shadow d-flex flex-column align-items-center justify-content-between mb-4">
                                                    <img class="img-fluid" src="<?php bloginfo('template_url') ?>/assets/images/kit.jpg" alt="kit">
                                                    <span class="--txt-green text-center">600 грн.</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--end.video-->
            </main>
            <!--include footer-->
            <?php get_footer()?>
        </div><!--end#app-->
    </body>

</html>