<?php
/*
    Template Name: about
*/
?>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Дельфин</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!-- <meta name="robots" content="index/noindex, follow/nofollow"> -->
        <meta name="robots" content="noindex, nofollow">
        <link rel="canonical" href="..." /> 
        <link rel="shortcut icon" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico" type="image/x-icon">
        <!-- styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">
        <!-- scripts -->
        <script src="<?php bloginfo('template_url') ?>/libs/libs.min.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/common.js"></script>
        <script type="module" src="<?php bloginfo('template_url') ?>/src/js/templates.js"></script>

    </head>
    <body>
        <aside class="modals" id="modals"></aside>

            <?php get_header() ?>
            <main>
                            <!--start.a-banner-->
                            <section class="a-banner --box-shadow"></section> <!--end.banner-->
                            <section class="a-banner_container">
                                <div class="container">
                                    <h1 class="--a-heading">О компании:</h1>
                                    <div class="row flex-column flex-md-row">
                                        <div class="col-md-2">
                                            <div class="a-banner_aside d-flex flex-row flex-md-column align-items-center mb-5">
                                                <div class="row">
                                                    <img class="a-banner_aside_doc --box-shadow" data-id="0" src="<?php bloginfo('template_url') ?>//assets/images/about/a-cert_1.jpg" alt="Свідоцтво на знак для товарів і послуг">
                                                </div>
                                                <div class="row">
                                                    <img class="a-banner_aside_doc --box-shadow" data-id="1" src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_2.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи">
                                                </div>
                                                <div class="row">
                                                    <img class="a-banner_aside_doc --box-shadow" data-id="3" src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_3.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи">
                                                </div>
                                                <div class="row">
                                                    <img class="a-banner_aside_doc --box-shadow" data-id="5" src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_4.jpg" alt="Свідоцтво про державну реєстрацію юридичної особи">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-10 mx-auto">
                                            <div class="a-banner_text d-flex flex-column align-items-center justify-content-center">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>
                                                            Разработки первого кораблика для рыбалки начались в 2010 году, весь пройденный путь нашей компанией можно узнать в статье «История рыболовного кораблика Дельфин».
                                                        </p>  
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <p>
                                                            На тот момент мы уже имели опыт работы с Китайской продукцией, которая нас не удовлетворяла по качеству, а европейские бренды были не доступны для нашего покупателя по цене. Нашей целью стало, получение отечественного продукта высокого качества, по доступной цене.<br>

                                                            Мы долгое время не предлагали на рынок нашу продукцию, работая над совершенством, только в 2014 году в продажу поступили первые модели Дельфин-2М, которые можно было купить в интернете. Мы сохранили традицию, все новые разработки также проходят тщательное тестирование перед выходом на рынок. 
                                                        </p>    
                                                    </div>
                                                    <div class="col-md-6">
                                                        <p>
                                                            Кораблики Дельфин разрабатываются ведущими Украинскими специалистами. Корпус кораблика, это авторская разработка, которая не имеет аналогов в мире. Электроника собрана на компактной плате, которую разработали наши специалисты. Разработчики постарались сделать кораблик максимально комфортным для эксплуатации и обслуживания.<br>

                                                            Вся комплектация проходит проверку на стенде, что позволяет использовать детали, не брендовых производителей, а их аналоги, ничем не уступающих по качеству, гарантия в 24 месяца, является лучшим подтверждением этому.
                                                        </p> 
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <p>
                                                            Продукция Дельфин является зарегистрированной ТМ «Дельфин», имеет сертификаты качества, и сертификаты авторских прав на производство и продажу своих разработок, для юридической защиты, в случае использования наших разработок другими производителями.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div> 
                                    </div> <!--end.container-->
                            </section>
                            <!--end.a-banner-->
                            <!--start.a-details-->
                            <section class="a-details d-flex flex-column justify-content-end">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12 col-lg-8">
                                            <h2 class="--box-shadow --a-heading">Производство:</h2>
                                            <p>
                                                Кораблики Дельфин изготавливаются на производственной базе ООО "Super Printer" (Свидетельство о гос. регистрации Серия A01 № 000216 от 16.03.2005) Юридический адрес 93400 Луганская обл., город Северодонецк, переулок Ломоносова, дом 8-а.
                                            </p>
                                            <p>
                                                Серийное производство позволяет существенно повысить качество изготавливаемой продукции. Используются комплектующие и материалы которые обладают высоким ресурсом, проверенным на практике. После сборки вся продукция проходит контроль качества, то что не заметил сборщик, будет отбраковано контролером. В случае обнаружения брака полностью вся партия отзывается со склада и производится повторная проверка всей парии, таким образом, мы исключаем продажу не проверенной продукции. Перед продажей все лодки проходят проверку на водоеме!
                                            </p>
                                        </div>
                                        <div class="col-md-12 col-lg-4">
                                            <h2 class="--box-shadow --a-heading">Сотрудничество:</h2>
                +                           <p>Приглашаем к сотрудничеству рыболовные магазины и интернет магазины.</p>
                +                           <p>Для получения более подробной информации свяжитесь с нами по телефону или электронной почте.</p>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    Телефоны:<br>
                                                    +38 (099) 044-4030<br>
                                                    +38 (098) 380-1111 
                                                </div>
                                                <div class="col-md-6">
                                                        E-mail:<br>
                                                        mail@delfin.ua                                        
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!--end.a-details-->
                        </main>
                        <!--include footer-->
                <?php get_footer() ?>
                <!--start.a-gallery-->
                <section class="a-gallery">
                    <div class="owl-carousel owl-theme owl-about">
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_1@x2.jpg" alt="Свідоцтво на знак для товарів і послуг"> </div>
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_2.1@x2.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи ст.1"> </div>
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_2.2@x2.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи ст.2"> </div>
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_3.1@x2.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи ст.1"> </div>
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_3.2@x2.jpg" alt="Висновок державної санітарно-епідеміологічної експеритизи ст.2"> </div>
                        <div> <img src="<?php bloginfo('template_url') ?>/assets/images/about/a-cert_4@x2.jpg" alt="Свідоцтво про державну реєстрацію юридичної особи"> </div>
                    </div>
                </section><!--end.a-gallery-->
        </body>
</html>
