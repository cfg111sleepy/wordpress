/**Горизонтальный скролл для галереи на странице новостей */

function scrollHorizontally(e) {
    e = window.event || e;
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    
    this.scrollLeft -= (delta*40); // Multiplied by 40
    e.preventDefault();
}
$(document).ready(function () {  
    if (document.getElementById('vGallery')){
        if (document.getElementById('vGallery').addEventListener) {
            // IE9, Chrome, Safari, Opera
            document.getElementById('vGallery').addEventListener("mousewheel", scrollHorizontally, false);
            document.getElementById('phGallery').addEventListener("mousewheel", scrollHorizontally, false);
            // Firefox
            document.getElementById('vGallery').addEventListener("DOMMouseScroll", scrollHorizontally, false);
            document.getElementById('phGallery').addEventListener("DOMMouseScroll", scrollHorizontally, false);
        } else {
            // IE 6/7/8
            document.getElementById('vGallery').attachEvent("onmousewheel", scrollHorizontally);
            document.getElementById('phGallery').attachEvent("onmousewheel", scrollHorizontally);
        }
    }
})


export default scrollHorizontally;
