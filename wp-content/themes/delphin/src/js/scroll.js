let scroll = {
    lastScrollTop: 0,
    init: function (){
        $(window).scroll(function(event){
            var st = $(this).scrollTop();
            //прячем хедер при скролле вниз
            if (st > scroll.lastScrollTop && document.scrollingElement.scrollTop >= 200){
                $('#header').addClass('hidden');
            } 
            //показываем хедер при скролле вверх
            else if (st < scroll.lastScrollTop && document.scrollingElement.scrollTop >= 200){
                $('#header').removeClass('hidden');
            }
            scroll.lastScrollTop = st;
        }); 
    }
}

$(document).ready(function () {  
    scroll.init();
})

export default scroll;
