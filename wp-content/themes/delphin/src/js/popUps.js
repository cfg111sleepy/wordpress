let popUps  = {
    init: function () {  
        $('.n-modal_vid').hide();
        $('.n-modal_ph').hide();
        $('.n-gallery_item').on('click', function(){
            popUps.popUp($('.n-modal_vid'));
        });
        $('.n-gallery_ph_item_img').on('click', function(){ 
            popUps.popUp($('.n-modal_ph'));
        });
    },
    popUp: function (selector) {  
        // show carousel + blur background     
        popUps.setBlur('#app', 10);
        selector.show();
        //dismiss owl-carousel on click outside the slide
        if (selector.css("display") == "block"){
            selector.on('click', function(){
                selector.hide();
                popUps.setBlur('#app', 0);
            })

            //close on esc        
            $(document).keyup(function(e) {
                if (e.keyCode === 27) {
                    selector.hide();
                    popUps.setBlur('#app', 0);
                }   
            });
        }
    },
    setBlur: function(ele, radius) {
        $(ele).css({
            "-webkit-filter": "blur("+radius+"px)",
            "filter": "blur("+radius+"px)"
        });
    }
}

$(document).ready(function(){
    popUps.init();
});

export default popUps;

   



    