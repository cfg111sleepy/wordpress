let customOwl = {
    init: function () {  
        this.about();
        this.cart();
        this.news();
    },
    about: function (){
        $('.a-banner_aside_doc').on('click', function(event){
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                dots:true,
                items:1,
                navText:["<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>", "<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>"],
            });
    
            // go to spesific slide
            let owl = $('.owl-carousel');
            $('.a-banner_aside_doc').click(function() {
                owl.trigger('to.owl.carousel', $(this).attr('data-id'));
            })
    
            //show carousel + blur background 
            $('#app').css({"filter":" blur(30px) opacity(90%)"});
            $('.a-gallery').show();
            event.stopPropagation();
    
            //dismiss owl-carousel on click outside the slide
            if ($('.owl-carousel').hasClass('owl-loaded')){
                $(window).click(function() {
                    $('.a-gallery').hide();
                    $('#app').css({"filter":" blur(0) opacity(100%)"});
                });
                //prevent closing on click on the slide
                $('.owl-item div img, .owl-nav button').click(function(event){
                    event.stopPropagation();
                });
            }
        }) 
    },
    cart: function () {  
        $('.owl-cart').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            items: 1,
            navText:["<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>", "<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>"],
        })
    },
    news: function () {  
        $('.n-gallery_ph_item_img').on('click', function(event){
            $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                center: true,
                nav:true,
                dots:true,
                items:1,
                navText:["<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>", "<svg xmlns='http://www.w3.org/2000/svg' viewBox='10207.126 3508.369 78.089 144.6'><path id='Path_292' data-name='Path 292'class='a-owl-nav' d='M10361.462,3487.783l67.925,67.925-52.8,52.8-21.044,21.044' transform='translate(-147 22)'/></svg>"],
            });
    
            // go to spesific slide
            let owl = $('.owl-carousel');
             $('.n-gallery_ph_item').click(function() {
                 owl.trigger('to.owl.carousel', $(this).attr('data-id'));
            })
    
            // //show carousel + blur background 
    
            $('#modalPh').show();
            event.stopPropagation();
    
            //dismiss owl-carousel on click outside the slide
            if ($('.owl-carousel').hasClass('owl-loaded')){
                $(window).click(function() {
                    $('#modalPh').hide();
                    //$('#app').css({"filter":" blur(0) opacity(100%)"});
                });
                //prevent closing on click on the slide
                $('.owl-item div img, .owl-nav button').click(function(event){
                    event.stopPropagation();
                });
            }
        }) 
    }
}

let nGallery = {
    images:[],
    methods: {
        getImages: function(){
            nGallery.images = $('.n-gallery_container_ph_inner').find('img').clone();
            nGallery.methods.renderImages();
        },
        renderImages: function(){
            let $gal = nGallery.images;
            $gal.each(function(index, image){
                let div = document.createElement('div');
                div.append(image);
                $('#modalPh').find('.owl-carousel').append(div);
            })
            nGallery.methods.giveId();
        },
        giveId: function (){
            $('.n-gallery_container_ph_inner').find('img').each(function(index, $item){
                
                //$item.attr("data-id", index);
            })
        }
    }
}

$(document).ready(function(){
    customOwl.init();
    nGallery.methods.getImages();
});

export default {nGallery, customOwl};