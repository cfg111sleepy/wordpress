let cart = {
    $cartItems: $('.cart_item'),
    init: function (){
        $('.cart_empty').hide();
        this.addCount();
        this.deleteItem();
        this.addRemove();
        this.renderPrice();
        this.overallPrice();
    },
    addCount : function(){
        //добавляем атрибут data-count с номером товара 
        this.$cartItems.each(function(index, $item) {
            $item.setAttribute('data-count', 1)
        })
    },
    deleteItem: function(){
        //удалить товар по клику на крестик
        $('.delete-btn').on('click', function(){
            if(cart.$cartItems.length>1){
                let $item = $(this).closest($('.cart_item'));
                $item.remove()
                if($('.cart').find('.cart_item').length == 0) {               
                    cart.cartEmpty();
                }
            }
        })
    },
    addRemove: function(){
        //добавление/удаление единиц товара
        $('.plus-minus-btn').on('click', function(){
            //получаем значиние data-count 
            const $container = $(this).siblings('.cart_item_count_val');
            const $cartItem = $(this).closest($('.cart_item'));
            let $getAttr = parseInt(($cartItem).attr('data-count'), 10);
            //+1
            if($(this).hasClass('addItem')){
                let $count = $getAttr + 1;
                let $setAttr = $cartItem.attr('data-count', $count);
            //-1
            } else if ($(this).hasClass('removeItem') && $getAttr > 1){
                let $count = $getAttr  - 1;
                let $setAttr = $cartItem.attr('data-count', $count);
            }
            //отображаем колличество товара
            let $newValue = $cartItem.attr('data-count');
            $container.text($newValue);
        })
    },
    renderPrice: function(){
        const $startPrice = parseInt($('.cart_side_price').html(), 10);
        $('.plus-minus-btn').on('click', function(){
            const $cartItem = $(this).closest($('.cart_item'));
            const $container = $cartItem.find('.cart_side_price');
            let $price = parseInt($('.cart_side_price').html(), 10);
            let $getAttr = parseInt(($cartItem.attr('data-count')), 10);
            //increment code
            if($(this).hasClass('addItem')){
                $container.text($price * $getAttr);
            //decrement code
            } else if ($(this).hasClass('removeItem') && $getAttr == 1){
                $price = $startPrice;
                $container.text($price);
            } else if ($(this).hasClass('removeItem') && $getAttr >= 1){
                $price -= $startPrice;
                $container.text($price);
            }
        })
    },
    overallPrice: function(){
        //получаем все стоимости всех товаров
        const $container = $('.cart_buttons_price');
        let cartItems = Array.from(document.getElementsByClassName('cart_side_price'));
        let prices = [];
        cartItems.forEach(function(item){
            prices.push(Number(item.innerHTML)); 
        })
        //суммирем стоимости
        let sum = prices.reduce(add, 0);
        function add(a, b) {
            return a + b;
        }
        //отображаем результат
        $container.html(sum);
        
        $('.plus-minus-btn, .delete-btn').on('click', function(){
            cart.overallPrice();
        })
    },
    cartEmpty: function(){
        $('.cart_empty').show();
        $('#cartAll').hide();
        $('.controlls_buy-btn').hide();
    }
}

$(document).ready(function(){
    cart.init();
})

export default cart;