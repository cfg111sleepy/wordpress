import templates from "./templates.js"; // подкючение хедера, футера, боковых кнопох
import timer from "./timer.js"; // таймер до 12:00
import scroll from "./scroll.js"; // спрятать/показать хедер при скролле
import customOwl from "./customOwl.js"; // инициализация owl-carousel + pop-up'ы
import horizScroll from "./horizScroll.js"; //горизонтальный скролл для галереи на странице новостей 
//import popUps from "./popUps.js"; // pop-up'ы для галерей
import setWidth from "./setWidth.js"; //Определение ширины контейнера галереи
import model from './contactInfo.js'; // json с информацией о магазинах
import contactPopUp from "./contactPopUp.js"; // всплывающее окно на странеице контактов
import cart from "./cart.js"; //скрипты для корзины


