let timer = {
    init: function () {
        this.toggle();
    },
    timer: function () {
        var date = new Date();
        var h12 = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12);
        var diff,
            hours,
            minutes,
            seconds;
        diff = (((h12 - Date.now()) / 1000) | 0);
        // Отображаем время до конца акции
        hours = (diff / 3600) | 0;
        minutes = ((diff % 3600) / 60) | 0;
        seconds = (diff % 60) | 0;
        //Добавляем "0" если число < 10 
        hours = hours < 10 ? "0" + hours : hours;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;
        //записывем результат
        let hrs = hours.toString().split("");
        let min = minutes.toString().split("");
        let sec = seconds.toString().split("");
        //отображаем результат в таймере
        $('#h1').text(hrs[0]);
        $('#h2').text(hrs[1]);

        $('#m1').text(min[0]);
        $('#m2').text(min[1]);

        $('#s1').text(sec[0]);
        $('#s2').text(sec[1]);
    },
    startTimer: function () {
        var date = new Date();
        var h12 = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12);
        if(date.getHours() >= 12) {
            h12.setDate(h12.getDate()+1);
        }
        h12 = h12.getTime();
        var diff,
            hours,
            minutes,
            seconds;
        this.timer();
        setInterval(this.timer, 1000);
    },
    toggle: function(){
        //проверяем время, показываем таймер в промежутке 00:00 - 12:00
        window.addEventListener("load", function () { 
            var date = new Date();
            if (date.getHours() < 12) { 
                //запускаем таймер, показываем контейнер
                timer.startTimer();
                $('.header-line').css({"display":"block"});
                $('.a-banner').css({"margin-top":"100px"});
                $('.n-header').css({"margin-top":"126px"});
                $('.bg-gradient').css({"margin-top":"126px"});
                $('.c-map').css({"margin-top":"126px"});
                $('.banner').css({"top":"100px"});
            }  
        });
    }
}

$(document).ready(function () {  
    timer.init();
});

export default timer;

