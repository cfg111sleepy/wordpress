let templates = {
    init: function(){
        this.header();
        this.footer();
        this.modals();
    },
    header: function(){
        $("#header").load("./template-parts/header.html");
    },
    footer: function(){
        $("#footer").load("./template-parts/footer.html");
    },
    modals: function(){
        $("#modals").load("./template-parts/modals.html");
    }
}

templates.init();

export default templates;
    

