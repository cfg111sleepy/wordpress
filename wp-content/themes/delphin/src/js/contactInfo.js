export default {
    data: [
        {
            "id": "Kiev",
            "store": "Продажа - Сервис 1",
            "city": "Киев",
            "number":"+38 (050) 511 511 8",
            "name": "Сергей"
        },
        {
            "id": "Sumy",
            "store": "Магазин «Carp Time»",
            "city":"Сумы",
            "address": "ул. Перекопская 9",
            "number": "+38 (050) 947 89 89",
            "name": "Сергей"     
        },
        {
            "id": "Kharkov",
            "store": "Магазин «Goodfish»",
            "city":"Харьков",
            "address": "ул. Григоровское шоссе 63",
            "number": "+38 (050) 051 03 81, +38 (098) 027 04 81",
            "name": "Аким",
            "image": ["./static/img/delfin_harkov.jpg"]
        },
        {
            "id": "Starobelsk",
            "store": "Магазин «Дом Рыбака»",
            "city":"Старобельск",
            "address": "ул. Кирова 1",
            "number": "+38 (066) 331 75 99",
            "name": "Артем" 
        },
        {
            "id": "Severodonetsk",
            "store": "Главный офис «Суперпринтер»",
            "city":"Северодонецк",
            "address": "пер. Ломоносова 8-а",
            "number": "+38 (098) 380 11 11, +38 (099) 044 40 30",
            "name": "Артем" ,
            "image": ["./static/img/severodonetsk.jpg"]
        },
        {
            "id": "Herson",
            "store": "Магазин «ПОКЛЕВКА»",
            "city":"Херсон",
            "address": "бульвар Мирный 4",
            "number": "+38 (067) 553 45 67, +38 (066) 188 57 55",
            "name": "Евгений" 
        },
        {
            "id": "Nikolaev",
            "store": "Магазин «Zebra»",
            "city":"Николаев",
            "address": "Чкалова 35б",
            "number": "+38 (093) 532 61 11",
            "name": "Стас" 
        },
        {
            "id": "Odessa",
            "store": "Магазин «Novitex»",
            "city":"Одесса",
            "address": "Малиновский рынок павильон 74-Б",
            "number": "+38 (096) 777 36 66, +38 (095) 645 75 75",
            "name": "<a href='novitex.com.ua'>novitex.com.ua</a>" 
        },
        {
            "id": "Kamenets",
            "store": "Магазин «Рыболов»",
            "city":"Каменец-Подольский",
            "address": "ул. Князей Кориатовичей 13",
            "number": "+38 ‎(096) 273 68 63",
        },
        {
            "id": "Khmelnitskiy",
            "store": "Магазин «Карп»",
            "city":"Хмельницкий",
            "address": "ул. Подольская 93",
            "name": "Павел",
            "addStore":"Продажа - Сервис",
            "addTel":"+38 (098) 625 75 30",
            "addName": "Максим"
        },
        {
            "id": "Moscow",
            "store": "Интернет Магазин",
            "city":"Россия, г. Москва",
            "number": "‎‎84954090896, ‎‎89099651350 WhatsApp, Viber",
            "name": "<a href='delfin-boat.ru'>delfin-boat.ru</a>",
        },
        {
            "id": "Lvov",
            "store": "«НАШ РИНОК» магазин №255",
            "city":"Львов",
            "address": "ул. Рапапорта 8",
            "number": "‎‎+38 (067) 938 93 08",
            "name": "Юра",
        },
        {
            "id": "Melitopol",
            "store": "Продажа - Сервис",
            "city":"Мелитополь",
            "number": "‎‎+38 (067) 11 33 998",
            "name": "Сергей",
        } 

    ]
}


