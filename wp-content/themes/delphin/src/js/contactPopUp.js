import model from './contactInfo.js';

let contact = {
    init: function () {  
        this.render();
        this.toggle(); 
        this.dots(); 
    },
    render: function () { 
        $('.cityBlock').on('click', function(e){ 
            //получаем id элемента по клику
            var blockID = $(this).attr('id');
            //ищем совпадение по id в модели
            model.data.forEach(function(key, index){
                if(blockID === model.data[index].id){
                    console.log('dd');
                    //выводим в блоки информацию из модели
                    $('#cityName').html(key.city);
                    $('#storeName').html(key.store);
                    $('#storeAddress').html(key.address);
                    $('#telNumber').html(key.number);
                    $('#name').html(key.name);
                    //дополнительная информация
                    $('#addStore').html(key.addStore);
                    $('#addTel').html(key.addTel);
                    $('#addName').html(key.addName);
                    if(key.image){
                        key.image.forEach(function(item){
                            let img = document.createElement('img');
                            img.setAttribute("src", key.image);
                            img.setAttribute("alt", `${key.store}, ${key.city}`);
                            $('#storeImg').append(img);
                        })
                    } 
                }
            });
        });
    },
    toggle: function () {  
        //открыть pop-up
        $('.cityBlock').on('click', function(){
            $('.c-map_overlay').show().animate(
                {top: "20%"});
        });
        //закрыть pop-up
        $('#contactClose').on('click', function(){
            $('.c-map_overlay').animate({
                top: "-600px"
            }, function(){$('.c-map_overlay').hide()});
            //очищаем содержимое pop-up'a
            $('#cityName').html('');
            $('#storeName').html('');
            $('#storeAddress').html('');
            $('#telNumber').html('');
            $('#name').html('');
            $('#addStore').html('');
            $('#addTel').html('');
            $('#addName').html('');
            $('#storeImg').empty();
        });
    },
    dots: function () {  
        let $currDot = $(this).find('.dot');
        let $prevDot = $currDot;
        $currDot.css({
            "background": "#707070",
            "height": "17px",
            "width": "17px",
            "border-radius": "8px"
        })
        $('#contactClose').on('click', function(){
            $prevDot.css({
                "background": "#FFFFFF",
                "height": "14px",
                "width": "14px",
                "border-radius": "7px"
            })
        })
    }
}

$(document).ready(function(){
    contact.init();
})

export default contact;
