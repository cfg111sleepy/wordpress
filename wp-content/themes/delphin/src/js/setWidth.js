/**Определение ширины контейнера галереи, чтобы элементы выстраивались в 2 ряда */

function setWidth() {
    let itemsV= $('.n-gallery_container_inner').find('.n-gallery_item').length;
    let itemsPh= $('.n-gallery_container_ph_inner').find('.n-gallery_ph_item').length;

    let widthV = ($('.n-gallery_item').width()+20)*itemsV/2-30;
    let widthPh = ($('.n-gallery_ph_item').width()+20)*itemsPh/2-30;

    
    $('.n-gallery_container_inner').css({"width":widthV});
    $('.n-gallery_container_ph_inner').css({"width":widthPh});
}

$(document).ready(function(){
    setWidth();
})

export default setWidth;