let vids = {
    data: [],
    allVids: [],
    previews: [],
    fetchData: function(){
        return fetch('https://www.googleapis.com/youtube/v3/search?key=AIzaSyC86OIkkWr234cCuX4noUiHifoQpW_1Ltk&channelId=UCIw7FHgd255alDFOJdLudLg&part=snippet,id&order=date&maxResults=4')
            .then(response => response.json())
            .then(json => this.data = json)
            .then(data => {
                this.data.items.forEach(element => {
                    vids.allVids.push(element)
                });
                this.renderImages();
            })
    },
    renderImages: function() {
        this.previews = Array.from(document.querySelectorAll("img.yt_preview"));
        let vid = 0;
        this.previews.forEach(element => {
            element.src = this.allVids[vid].snippet.thumbnails.medium.url;
            element.alt = this.allVids[vid].snippet.title;
            element.parentNode.href = `https://www.youtube.com/embed/${this.allVids[vid].id.videoId}`;
            vid++;
        });
    }
}

vids.fetchData();

export default vids;